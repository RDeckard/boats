# frozen_string_literal: true

class CreateBoats < ActiveRecord::Migration[6.0]
  def change
    create_table :boats do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
