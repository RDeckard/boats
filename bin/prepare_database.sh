#!/usr/bin/env bash

# We'll need database managment but for now,
# `bin/rails db:reset || exit 1` do all of that already :D.

echo "Recreating a DB..."
bin/rails db:drop || exit 1
bin/rails db:create || exit 1

# echo "Loading schema..."
# bin/rails db:schema:load || exit 1

echo "Running migrations..."
bin/rails db:migrate || exit 1

echo "Seeding DB..."
bin/rails db:seed || exit 1
