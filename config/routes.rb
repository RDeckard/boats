# frozen_string_literal: true

Rails.application.routes.draw do
  resources :boats
  devise_for :users
  mount Sidekiq::Web => '/sidekiq' # monitoring console
  root 'boats#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
