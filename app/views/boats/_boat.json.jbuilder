# frozen_string_literal: true

json.extract! boat, :id, :name, :description, :created_at, :updated_at
json.url boat_url(boat, format: :json)
